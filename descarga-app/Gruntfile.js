module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '* http://<%= pkg.homepage %>/\n' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
      '<%= pkg.author.name %>; Licensed MIT */\n'
    },

    src: {
      sass: {
        main: 'app/assets/sass/style.scss',
        files: ['app/assets/sass/**/*.scss'],
        cache: ['app/assets/.sass-cache', '.sass-cache']
      },
      js: {
        main: 'app/assets/js/script_aux.js',
        modules: 'app/assets/js/modules/*.js',
        files: ['app/assets/js/**/*.js'],
        vendor: [
          'bower_components/bootstrap/dist/js/bootstrap.js',
          'bower_components/materialize/dist/js/materialize.js',
          'bower_components/form-validator/dist/jquery.form-validator.js',
          'bower_components/lity/dist/lity.js',
          'bower_components/wow/dist/wow.min.js',
          'bower_components/sequencejs/scripts/imagesloaded.pkgd.min.js',
          'bower_components/sequencejs/scripts/hammer.min.js',
          'bower_components/sequencejs/scripts/sequence.min.js',
          'bower_components/velocity/velocity.js',
          'bower_components/owl.carousel/dist/owl.carousel.min.js',
          'bower_components/masonry/dist/masonry.pkgd.min.js',
          'bower_components/stacktable/stacktable.js',
          'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
          'bower_components/bootstrap-select/dist/js/i18n/defaults-es_ES.js',
          'bower_components/moment/min/moment.min.js',
          'bower_components/moment/locale/es.js',
          'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
          'bower_components/parsleyjs/dist/parsley.min.js',
          'bower_components/parsleyjs/dist/i18n/es.js',
          'bower_components/geocomplete/jquery.geocomplete.min.js',
          'bower_components/FitText.js/jquery.fittext.js',
          'bower_components/waterwheel-carousel/js/jquery.waterwheelCarousel.js',
          'bower_components/sweetalert2/dist/sweetalert2.min.js',
          'bower_components/emojionearea/dist/emojionearea.min.js',
          'bower_components/mobile-detect/mobile-detect.min.js'
        ]
      },
      css: {
        main: 'app/assets/css/style.css',
        vendor: [
         'bower_components/bootstrap/dist/css/bootstrap.css',
          'bower_components/owl.carousel/dist/assets/owl.carousel.css',
          'bower_components/owl.carousel/dist/assets/owl.theme.default.css',
          'bower_components/bootstrap-select/dist/css/bootstrap-select.css',
          'bower_components/materialize/dist/css/materialize.css',
          'bower_components/lity/dist/lity.css',
          'bower_components/wow/css/libs/animate.css',
          'bower_components/sequencejs/css/sequence-theme.intro.css',
          'bower_components/stacktable/stacktable.css',
          'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
          'bower_components/sweetalert2/dist/sweetalert2.min.css',
          'bower_components/emojionearea/dist/emojionearea.min.css'  /* BREAKS IOS */
        ]
      },
      dist: {
        css: 'app/assets/css/style.css',
        js: 'app/assets/js/script_aux2.js',
        cssmin: 'app/assets/css/style.min.css',
        jsmin: 'app/assets/js/script.min.js',
        folder: 'dist'
      }
    },

    sass: {
      main: {
        files: {
          '<%=src.css.main%>': '<%=src.sass.main%>'
        }
      }
    },

    jshint: {
      files: ['Gruntfile.js', '<%= src.js.main %>'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },

    concat: {
      css: {
        src: ['<%= src.css.vendor %>', '<%= src.css.main %>'],
        dest: '<%= src.dist.css %>'
      },
      js: {
        src: ['<%= src.js.vendor %>', '<%= src.js.modules %>', '<%= src.js.main %>'],
        dest: '<%= src.dist.js %>'
      }
    },

    cssmin: {
      options: {
        rebase: false
      },
      main: {
        src: '<%= src.dist.css %>',
        dest: '<%= src.dist.cssmin %>'
      }
    },

    uglify: {
      options: {
        compress: false,
        banner: '<%= meta.banner %>',
        preserveComments: false
      },
      build: {
        src: '<%= src.dist.js %>',
        dest: '<%= src.dist.jsmin %>'
      }
    },

    clean: {
      cache: '<%=src.sass.cache%>',
      dist: '<%= src.dist.folder%>'
    },

    watch: {
      sass: {
        files: ['<%=src.sass.files%>'],
        tasks: ['css', 'postcss', 'clean:cache']
      },
      js: {
        files: ['<%=src.js.main%>', '<%=src.js.modules%>'],
        tasks: ['js']
      }
    },

    php2html: {
      default: {
        options: {
          processLinks: true,
          htmlhint: [
            {
              'tagname-lowercase': true,
              'attr-lowercase': true,
              'attr-value-double-quotes': true,
              'doctype-first': true,
              'tag-pair': true,
              'spec-char-escape': true,
              'id-unique': true,
              'src-not-empty': true
            }
          ]
        },
        files: [
          {expand: true, cwd: './', src: ['app/*.php'], dest: 'dist', flatten: true, ext: '.html'}
        ]
      }
    },

    copy: {
      dist: {
        files: [
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/fonts/',
            src: ['**'],
            dest: 'dist/assets/fonts/'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: false,
            cwd: 'app/assets/js/tinymce',
            src: ['**'],
            dest: 'dist/assets/js/tinymce'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/css/',
            src: ['*.css'],
            dest: 'dist/assets/css'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/sounds',
            src: ['**'],
            dest: 'dist/assets/sounds'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['script.min.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['script_aux.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['dev.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['rating.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['sweetAlert.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['geo.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['video.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['player.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'app/assets/js/',
            src: ['audio-waves.js'],
            dest: 'dist/assets/js'
          },
          {
            expand: true,
            filter: 'isFile',
            flatten: false,
            cwd: 'app/assets/images/',
            src: ['**'],
            dest: 'dist/assets/images'
          }
        ]
      }
    },
     browserSync: {
      default_options: {
        bsFiles: {
          src: [
            "app/assets/css/*.css",           
            "app/assets/js/*.js",                                   
            "app/*.php"            
          ]
        },
        options: {
          watchTask: true,
          proxy: "http://dufry.local.com",
          livereload:true         
        }
      }
    },

    replace: {
      example: {
        src: ['dist/**/*.html'],
        overwrite: true,
        replacements: [
          {
            from: /\/app\/assets\//g,
            to: 'assets/'
          },
          {
            from: /\/app\/pages\//g,
            to: ''
          }
        ]
      },
      jquery: {
        src: ['dist/**/*.js'],
        overwrite: true,
        replacements: [
          {
            from: /\$/g,
            to: 'jQuery'
          }
        ]
      }
    },

    postcss: {
      options: {
        map: true, // inline sourcemaps

        // or
        map: {
            inline: false, // save all sourcemaps as separate files...
            annotation: 'app/assets/css/' // ...to the specified directory
        },

        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: '<%= src.dist.css %>'
      }
    }    
  });

  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-php2html');
  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-postcss');

  // Default task(s).
  grunt.registerTask('css', ['sass', 'concat:css', 'cssmin']);
  grunt.registerTask('js', ['concat:js', 'uglify']);
  grunt.registerTask('build', ['css', 'js', 'clean:dist', 'php2html', 'copy', 'replace']);
  grunt.registerTask('default', ['browserSync', 'watch', 'css', 'js', 'clean']);
};
