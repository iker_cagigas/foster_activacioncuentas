<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Descárgate la app</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
		<div class="c-header">
			<div class="c-header__content">
				<img src="assets/images/logo.svg" class="c-header__logo">
			</div>
		</div>
			
	<div class="c-app-page">


		<div class="c-app">
			<div class="c-app__content">
				<div class="c-app__info">
					<h1 class="c-app__info-title">Descárgate la <span>app</span></h1>

					<div class="c-app__info-text">
						<p>Únete a la comunidad exclusiva donde disfrutar al máximo de la experencia de Foster's Hollywood con un montón de ventajas, descuentos y beneficios:</p>
					</div>

					<div class="c-app__info-links">
						<a href="https://itunes.apple.com/us/app/fosters-hollywood/id589061526" class="c-app__info-iphone">
							<img src="assets/images/app-store.svg" alt="App Store">
						</a>

						<a href="https://play.google.com/store/apps/details?id=com.zena.Fosters" class="c-app__info-android">
							<img src="assets/images/google-play.svg" alt="Google play">
						</a>
					</div>

					<div class="c-app__legal">
						<div class="c-app__legal-links">
							<a href="https://fostershollywood.es/fosterianos">Condiciones generales de Fosterianos</a>
							<a href="https://fostershollywood.es/informacion-adicional-proteccion-datos-fosterianos">Información adicional protección datos Fosterianos</a>
						</div>

						<div class="c-app__legal-text">
							<p>El 25/05/2018 comenzará a aplicarse una nueva norma en materia de protección de datos personales. Se trata del Reglamento (UE) 2016/679 General de Protección de Datos que aplica por igual a todos los países de la U.E. y busca reforzar tu derecho a la información que vas a tener sobre cómo y para que vamos a utilizar tus datos personales. En cumplimiento de esta nueva normativa debemos modificar nuestra política de privacidad. A tales efectos puedes leer la "Información adicional sobre protección de datos", referida al Programa Fosterianos, que se encuentra en el presente apartado.</p>
						</div>
					</div>					
				</div>

				<div class="c-app__image">
					<img src="assets/images/app.png" alt="Descárgate la app">
				</div>
			</div>
		</div>
	</div>

	<div class="c-footer">
		<div class="c-footer__content">
			<div class="c-footer__logo">
				<img src="assets/images/logo-footer.svg" alt="Foster's Hollywood">	
			</div>
			
			<div class="c-footer__info">
				<ul class="c-footer__links">
					<li><a href="https://fostershollywood.es/aviso-legal-y-condiciones-de-uso">Aviso legal e Información general</a></li>
					<li><a href="https://fostershollywood.es/informacio-general">Información adicional Protección de datos</a></li>
					<li><a href="https://fostershollywood.es/informacion-sobre-cookies">Información cookies</a></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
