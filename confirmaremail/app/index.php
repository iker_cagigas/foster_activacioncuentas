<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Recuperar contrase&ntilde;a</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div class="c-header">
		<div class="c-header__content">
			<img src="assets/images/fosterianos.svg" class="c-header__logo">
		</div>
	</div>

	<div class="c-background">

	</div>
	<div class="c-tarjeta">
		<div class="c-tarjeta__content">
			<h1 class="c-tarjeta__title">MUCHAS GRACIAS POR&nbsp;<span class="c-tarjeta__title--font">CONFIRMAR TU EMAIL</span></h1>
			<p class="c-tarjeta__description c-tarjeta__description--regular">
				<span>Ahora que ya eres Fosteriano empezar&aacute;s a disfrutar de todas las ventajas que tenemos para ti.</span>
			</p>
		</div>
	</div>

	<div class="c-footer">
		<div class="c-footer__content">
			<img src="assets/images/logoFoster.svg" class="c-footer__logo">
		</div>
	</div>
</body>
</html>
