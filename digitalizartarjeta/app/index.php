<?php

$error = null;

if (isset($_GET["erroremail"])) {
	$error = $_GET["erroremail"];
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Digitalizar Tarjeta</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
</head>
<body>
	<div class="c-header">
		<div class="c-header__content">
			<img src="assets/images/fosterianos.svg" class="c-header__logo">
		</div>
	</div>

	<div class="c-background">

	</div>
	<div class="c-tarjeta">
		<div class="c-tarjeta__content">
			<h1 class="c-tarjeta__title">p&aacute;sate a la&nbsp;<span class="c-tarjeta__title--font">tarjeta digital</span></h1>
			<p class="c-tarjeta__description">Las ventajas de Fosterianos son tantas, que es mejor tenerlas siempre a mano. </p>
			<p class="c-tarjeta__description c-tarjeta__description--regular u-hidden__mobile">&iquest;A qu&eacute; esperas para pasarte a la tarjeta digital?</p>

			<form id="tarjetaAppAlta" method="post" action="descarga_app.html">
				<div class="c-tarjeta__blocks c-tarjeta__blocks--padding">
					<div class="c-tarjeta__block">
						<div class="c-tarjeta__block--width input-field">
							<label for="nombre">NOMBRE*</label>
							<input required="required" type="text" name="modelo.nombre" id="nombre" size="20">
						</div>					

						<div class="c-tarjeta__block--width input-field">
							<label for="apellido1">APELLIDO 1*</label>
							<input required="required" type="text" name="modelo.apellido1" size="20" id="apellido1">
						</div>

						<div class="c-tarjeta__block--width input-field">
							<label for="apellido2">APELLIDO 2*</label>
							<input required="required" type="text" name="modelo.apellido2" size="20" id="apellido2">
						</div>

						<div class=" c-tarjeta__wf-calendar c-tarjeta__block--width input-field">
							<label for="date">FECHA NACIMIENTO*</label>
							<input required="required" id="date" name="modelo.fecnacim" type="text" class="js-datepicker">
						</div>
						<div class="c-tarjeta__blocks c-tarjeta__block--width c-tarjeta__block--justify">
							<div class="c-tarjeta__block c-tarjeta__block--column-left input-field c-tarjeta__block-select">
								<select required="required" class="js-example-basic-single" id="js-example-basic-hide-search" name="documento">
									<option value="" disabled selected>TIPO DE DOCUMENTO*</option>
									<option value="NIF">NIF</option>
									<option value="PASAPORTE">PASAPORTE</option>
								</select>
							</div>
							<div class="c-tarjeta__block c-tarjeta__block--column-right input-field">
								<label id="documento">N&Uacute;MERO DE DOCUMENTO*</label>
								<input required="required" type="text" name="modelo.numdoc" id="documento" value="" size="32">
							</div>
						</div>

						<div class="c-tarjeta__block--width input-field c-tarjeta__block-select">
							<select required="required" class="js-example-basic-single1" id="js-example-basic-hide-search1">
								<option value="" disabled selected>SEXO*</option>
								<option value="NIF">MASCULINO</option>
								<option value="PASAPORTE">FEMENINO</option>
							</select>
						</div>

					</div>

					<div class="c-tarjeta__block">

						<div class="c-tarjeta__wf-mobile c-tarjeta__block--width input-field">
							<label for="movil">M&Oacute;VIL*</label>
							<input type="tel" name="modelo.movil" required="required" id="movil">
						</div>
						<div class="c-tarjeta__wf-email c-tarjeta__block--width input-field <?php if ($error != null){ echo "c-error"; } ?>">
							<label for="email">Tu email*</label>
							<input required="required" name="modelo.email" class="" type="email" size="60" id="email">
							<?php if ($error != null){ echo '<div><div id="email-error" class="error" style="display: block;">Error.</div></div>'; } ?>
						</div>

						<div class="c-tarjeta__block--width input-field">
							<label for="cp">C&Oacute;DIGO POSTAL*</label>
							<input type="text" id="codigopostal" name="modelo.codpostal" maxlength="5" required="required" id="cp">
						</div>

						<div class="c-tarjeta__wf-pass c-tarjeta__block--width input-field">
							<label for="password1">NUEVA CONTRASE&Ntilde;A*</label>
							<input id="password1" required="required" class="" name="modelo.pwd" type="password" size="60">
						</div>

						<div class="c-tarjeta__wf-pass c-tarjeta__block--width input-field">
							<label id="password2">REPITE LA CONTRASE&Ntilde;A*</label>
							<input required="required" class="" name="password2" type="password" size="60" id="password2">
						</div>


					</div>
				</div>
				<div class="c-tarjeta__submit">
					<input type="submit" value="Enviar" />
				</div>
			</form>

		</div>
	</div>
	<div class="c-proteccion">
		<div class="c-proteccion__content">
			<h1 class="c-proteccion__title">Informaci&oacute;n b&aacute;sica sobre&nbsp;<span class="c-proteccion__title--font">protecci&oacute;n de datos</span></h1>
			<div class="c-proteccion__block">
				<div class="c-proteccion__left">
					<h4 class="c-proteccion__subtitle">Responsable</h4>
					<p class="c-proteccion__text">
						<span class="c-proteccion--bold">Food Service Project, S.L. (&ldquo;FSP&rdquo;)</span> Camino de la Zarzuela n&ordm;1, 28023, Madrid.
					</p>
					<h4 class="c-proteccion__subtitle">Finalidades principales y legitimaci&oacute;n</h4>
					<p class="c-proteccion__text">
						Gestionar la participaci&oacute;n en el programa Fosterianos y env&iacute;o de recompensas y comunicaciones referidos al programa en el aplicativo de la app basado en la ejecuci&oacute;n de un contrato, env&iacute;o de encuestas y/o publicidad basada en perfiles de comportamiento referentes a productos, promociones o servicios de FSP, tanto de Fosterianos como de Foster&acute;s Hollywood, basado en el inter&eacute;s leg&iacute;timo de FSP y atenci&oacute;n a las quejas o reclamaciones o sugerencias que puedan surgir respecto del programa en base a la participaci&oacute;n en el programa Fosterianos.
					</p>
				</div>
				<div class="c-proteccion__right">
					<h4 class="c-proteccion__subtitle">Destinatarios</h4>
					<p class="c-proteccion__text">
						Franquiciados de Foster&acute;s Hollywood para atender reclamaciones pertenecientes a los mismos.
					</p>
					<h4 class="c-proteccion__subtitle">Derechos</h4>
					<p class="c-proteccion__text">
						Podr&aacute; acceder, rectificar, suprimir, oponerse y limitarse a determinados tratamientos, as&iacute; como portar los mismos a trav&eacute;s del correo electr&oacute;nico <a href="mailto:fosterianos@fostershollywood.es">fosterianos@fostershollywood.es</a>.
					</p>
					<h4 class="c-proteccion__subtitle">Informaci&oacute;n adicional</h4>
					<p class="c-proteccion__text">
						Puede consultar la informaci&oacute;n adicional en <a target="_blank" href="https://fostershollywood.es/fosterianos">https://fostershollywood.es/fosterianos</a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="c-footer">
		<div class="c-footer__content">
			<img src="assets/images/logoFoster.svg" class="c-footer__logo">
		</div>
	</div>

	<!-- Latest compiled and minified JavaScript -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script type="text/javascript" src="assets/js/vendor/materialize/bin/materialize.min.js"></script>
	<script type="text/javascript" src="assets/js/script.min.js"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
	<script type="text/javascript" src="assets/js/tarApp.js"></script>	
</body>
</html>
