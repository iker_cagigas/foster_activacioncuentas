$(document).ready(function() {
  $('.js-example-basic-single').select2();
  $("#js-example-basic-hide-search").select2({
    minimumResultsForSearch: Infinity,
  });

  $('.js-example-basic-single1').select2();
  $("#js-example-basic-hide-search1").select2({
    minimumResultsForSearch: Infinity
  });

/*
  $(".js-datepicker").datetimepicker({
    format: "DD/MM/YYYY",
    locale: "es"
  });
*/  
  currentYear = (new Date()).getFullYear();

  $('.js-datepicker').datepicker({
    firstDay: 1,
    autoClose: true,
    format: 'dd/mm/yyyy',
    yearRange: [currentYear-100, 2018],
    i18n: {
      weekdaysAbbrev: ['DO','LU','MA','MI','JU','VI','SÁ'],
      months: [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiempre',
        'Octubre',
        'Noviembre',
        'Diciembre'
      ],
      monthsShort: [
        'Ene',
        'Feb',
        'Mar',
        'Abr',
        'May',
        'Jun',
        'Jul',
        'Ago',
        'Sep',
        'Oct',
        'Nov',
        'Dic'     
      ],
      weekdays: [
        'Domingo',
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado'      
      ],
      weekdaysShort: [
        'Do',
        'Lu',
        'Ma',
        'Mi',
        'Ju',
        'Vi',
        'Sa'      
      ] 
    },
  });
});


function checkVerCaracteres(){
  var upass = document.getElementById('password1');
  var toggleBtn = document.getElementById('ver');
  if(upass.type == "password" &&  toggleBtn.checked){
    toggleBtn.checked = false;
  }
  return true;
}

(function( factory ) {
  if ( typeof define === "function" && define.amd ) {
    define( ["jquery", "../jquery.validate"], factory );
  } else if (typeof module === "object" && module.exports) {
    module.exports = factory( require( "jquery" ) );
  } else {
    factory( jQuery );
  }
}(function( $ ) {


  $.validator.addMethod("dniCheck", function(value, element) {
    if(/^([0-9]{8})*[a-zA-Z]+$/.test(value)){
      var numero = value.substr(0,value.length-1);
      var let = value.substr(value.length-1,1).toUpperCase();
      numero = numero % 23;
      var letra='TRWAGMYFPDXBNJZSQVHLCKET';
      letra = letra.substring(numero,numero+1);
      if (letra==let) return true;
      return false;
    }
    return this.optional(element);
  }, "DNI no v&aacute;lido");


//Mobile phone number validation
jQuery.validator.addMethod("mobile", function(value, element) {
  var length = value.length;
  value = value.toString().replace(/\s/g, '');
  var mobile = /^[679]{1}[0-9]{8}$/
  return this.optional(element) || (length == 9 && mobile.test(value));
}, "Tel&eacute;fono movil no es v&aacute;lido");




//Phone number validation
jQuery.validator.addMethod("phone", function(value, element) {
  var tel = /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/;
  return this.optional(element) || (tel.test(value));
}, "Telephone number format error");

//Postal code verification
jQuery.validator.addMethod("zipCode", function(value, element) {
  var tel = /^[0-9]{6}$/;
  return this.optional(element) || (tel.test(value));
}, "Postal code format error");

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
 $.extend( $.validator.messages, {
   required: "Este campo es obligatorio.",
   remote: "Por favor, rellena este campo.",
   email: "Por favor, escribe una direcci&oacute;n de correo v&aacute;lida.",
   mobile: "Por favor, escribe una n&uacute;mero de m&oacute;vil v&aacute;lido.",
   url: "Por favor, escribe una URL v&aacute;lida.",
   date: "Por favor, escribe una fecha v&aacute;lida.",
   dateISO: "Por favor, escribe una fecha (ISO) v&aacute;lida.",
   //dateITA: "Por favor, escribe una fecha v&aacute;lida.",
   number: "Por favor, escribe un n&uacute;mero v&aacute;lido.",
   digits: "Por favor, escribe s&oacute;lo d&iacute;gitos.",
   creditcard: "Por favor, escribe un n&uacute;mero de tarjeta v&aacute;lido.",
   equalTo: "Por favor, escribe el mismo valor de nuevo.",
   extension: "Por favor, escribe un valor con una extensi&oacute;n aceptada.",
   maxlength: $.validator.format( "Por favor, no escribas m&aacute;s de {0} caracteres." ),
   minlength: $.validator.format( "Por favor, no escribas menos de {0} caracteres." ),
   rangelength: $.validator.format( "Por favor, escribe un valor entre {0} y {1} caracteres." ),
   range: $.validator.format( "Por favor, escribe un valor entre {0} y {1}." ),
   max: $.validator.format( "Por favor, escribe un valor menor o igual a {0}." ),
   min: $.validator.format( "Por favor, escribe un valor mayor o igual a {0}." ),
   nifES: "Por favor, escribe un NIF v&aacute;lido.",
   nieES: "Por favor, escribe un NIE v&aacute;lido.",
   cifES: "Por favor, escribe un CIF v&aacute;lido."
 } );

}));
// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#tarjetaAppAlta").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      "modelo.nombre": {
        required: true
      },
      "modelo.apellido1":{
        required: true
      },
      "modelo.apellido2":{
        required: true
      },
      "modelo.fecnacim": {
        required: true
      },
      "modelo.numdoc":{
        required:true,
        dniCheck:true
      },
      "modelo.codpostal":{
        required: true,
        number:true,
        minlength: 5
      },
      "modelo.movil": {
       required: true,
       mobile: true
     },
     "modelo.email": {
      required: true,
      email: true
    },
    "modelo.pwd": {
      required: true,
      minlength: 5
    },
    "password2": {
      required: true,
      minlength: 5,
      equalTo: "#password1"
    }
  },

  highlight: function(element) {
      $(element).parent('.input-field').addClass('c-error');
  },
  unhighlight: function(element) {
      $(element).parent('.input-field').removeClass('c-error');
  },

  ignore: [],

  errorElement: "div",
        wrapper: "div",  // a wrapper around the error message
        errorPlacement: function(error, element) {
         element.after(error);
         offset = element.offset();
       },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
       form.submit();
     }
   });
});

