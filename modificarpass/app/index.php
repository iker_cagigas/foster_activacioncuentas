<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Modificar Pass</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div class="c-header">
		<div class="c-header__content">
			<img src="assets/images/fosterianos.svg" class="c-header__logo">
		</div>
	</div>

	<div class="c-background">

	</div>
	<div class="c-tarjeta">
		<div class="c-tarjeta__content">
			<h1 class="c-tarjeta__title">modificar&nbsp;<span class="c-tarjeta__title--font">contrase&ntilde;a</span></h1>
			<p class="c-tarjeta__description">No se admiten signos de puntuaci&oacute;n, ni otros caracteres que no sean letras no acentuadas o n&uacute;meros.</p>
			<p class="c-tarjeta__description c-tarjeta__description--regular">Debes introducir al menos 6 caracteres.</p>
			<form >
				<div class="c-tarjeta__blocks c-tarjeta__blocks--padding">
					<div class="c-tarjeta__block">

						<div class="c-tarjeta__wf-pass c-tarjeta__block--width">
							<label class="element-invisible" >NUEVA CONTRASE&Ntilde;A*</label>
							<input required="required" class="" placeholder="NUEVA CONTRASE&Ntilde;A*" type="password" size="60">
						</div>

						<div class="c-tarjeta__wf-pass c-tarjeta__block--width">
							<label class="element-invisible" >REPITE LA CONTRASE&Ntilde;A*</label>
							<input required="required" class="" placeholder="REPITE LA CONTRASE&Ntilde;A*" type="password" size="60">
						</div>
						<div class="c-tarjeta__block--width">
							<input type="checkbox" id="ver" name="ver"
							value="ver" />
							<label for="ver" class="c-tarjeta__block--checkbox">Ver contrase&ntilde;a</label>
						</div>
						<div class="c-tarjeta__submit">
							<input type="submit" value="GUARDAR CONTRASE&Ntilde;A" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="c-footer">
		<div class="c-footer__content">
			<img src="assets/images/logoFoster.svg" class="c-footer__logo">
		</div>
	</div>
</body>
</html>
